<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <!--jQuery-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <!--UI theme roller-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/stylesheets/custom-theme/jquery-ui-1.8.24.custom.css" />

    <!--Styles-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/stylesheets/960.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style.css" />

    <!--prefix free-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/prefixfree.min.js"></script>

    <!--Modernizer-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.1.min.js"></script>

    <!--Only Mac - Safari Class-->
    <script type="text/javascript">
    jQuery(function(){
        // console.log(navigator.userAgent);
        /* Adjustments for Safari on Mac */
        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1) {
            jQuery('html').addClass('mac'); // provide a class for the safari-mac specific css to filter with
        }
    });
    </script>

    <!--Selectivizr-->
    <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="js/selectivizr.js"></script>
        <noscript><link rel="stylesheet" href="stylesheets/theme.css" /></noscript>
    <![endif]-->

    <!--[if IE]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if gte IE 9]>
        <style type="text/css">
         .gradient {
            filter: none;
        }
     </style>
    <![endif]-->

	<title>Ifeanyi Ubah</title>

<?php wp_head(); ?>
</head>
<body>

<div id="wrap">
	<header id="header" class="container_24"> 
        <h1 id="logo"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ifeanyi-uba-logo.png" alt="Ifeanyi Ubah Foundation"></a></h1>
        <div id="top-social-network" class="grid_16 prefix_8">
            <ul id="follow-us" class="grid_8">
                Follow us:
                <?php
                if (is_active_sidebar('header-social-media')) :
                    dynamic_sidebar('header-social-media');
                endif;
                ?>
            </ul> 
            <div id="search" class="grid_7 prefix_1">
                <?php get_search_form(); ?>
            </div>
        </div>
        <div id="top-newsletter" class="grid_16 prefix_8">
            <h2 class="grid_5 alpha">Sign Up</h2>
            <?php
            if (is_active_sidebar('header-newsletter')) :
                dynamic_sidebar('header-newsletter');
            endif;
            ?>            
        </div>
	</header>
    
    <div id="container">
    	<div id="main-banner" <?php post_class(); ?>>
            <div class="container_24">
                <div id="text" class="grid_11">
                    <?php
                    if (is_active_sidebar('header-main-text')) :
                        dynamic_sidebar('header-main-text');
                    endif;
                    ?>                    
                </div>
            </div>
        </div>
        <nav>
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
        </nav>