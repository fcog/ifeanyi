<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>
<section id="summarize">
    <div class="container_24">

        <h3>Ifeanyi Ubah Foundation Programmes</h3>

        <?php
        $wp_query->query(array('post_type' => 'programmes', 'posts_per_page' => '3'));
        while ($wp_query->have_posts()): $wp_query->the_post(); ?>
    
        <article>
            <h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
            <?php if (has_post_thumbnail()): ?><?php the_post_thumbnail(); ?><?php endif ?>
            <?php the_excerpt(); ?>
            <a href="<?php echo get_permalink(); ?>" class="read-more">Read More</a>
        </article>

        <?php endwhile; ?>

    </div>
</section>
<section id="news">
    <div class="container_24">
        <h3>In The News</h3>

        <?php
        $wp_query->query(array('post_type' => 'news', 'posts_per_page' => '3'));
        while ($wp_query->have_posts()): $wp_query->the_post(); ?>

        <article>
            <h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
            <span><time><?php the_time('m-d-Y'); ?></time></span>
            <?php the_excerpt(); ?>
            <a href="<?php echo get_permalink(); ?>" class="read-more">Read More</a>
        </article>

        <?php endwhile; ?>
    </div>
</section>
<section id="blog">
    <div class="container_24">
        <h3>Blog</h3>

        <?php
        $wp_query->query(array('post_type' => 'post', 'posts_per_page' => '3'));
        while ($wp_query->have_posts()): $wp_query->the_post(); ?>

        <article>
            <?php if (has_post_thumbnail()): ?><?php the_post_thumbnail('newsroom-thumb'); ?><?php endif ?>
            <h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
            <?php the_excerpt(); ?>
        </article>

        <?php endwhile; ?>

    </div>
</section>
<section id="video">
    <div class="container_24">
        <div id="video-box">
            <h3>Featured Video</h3>
            <?php
            if (is_active_sidebar('homepage-video')) :
                dynamic_sidebar('homepage-video');
            endif;
            ?>
        </div>
        <div id="facebook-like">
            <h4>Facebook</h4>
            <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fifeanyiubahfndn&amp;width=240&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=705786149435585" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:258px;" allowTransparency="true"></iframe>
        </div>
        <div id="twitter-feed">
            <a class="twitter-timeline" href="https://twitter.com/IfeanyiUbahFndn" data-widget-id="367488306558144515" width="240">Tweets por @Ubah4Gov</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
    </div>
</section>
<section id="voice">
    <div class="container_24">
        <h3>What Others Are Saying</h3>
        <?php 
		$comments = get_comments(array('post_id' => '12', 'number'=>'2', 'status'=>'approve'));
		foreach($comments as $comment) :
		?>
        <div id="quote" class="grid_12">
            <?php echo get_avatar($comment->user_id) ?>
            <p>“<?php echo $comment->comment_content ?>” <span><?php echo $comment->comment_author ?></span>
            </p>
        </div>
    	<?php endforeach ?>
    </div>
</section>
<?php get_footer(); ?>