    </div>
</div>

<footer id="footer">
    <div class="container_24">  
        <div id="bottom-newsletter" class="grid_24">
            <h2>Newsletter</h2> <p>Subscribe to our news and get the latest information from Ifeanyi Ubah</p>
            <?php
            if (is_active_sidebar('footer-newsletter')) :
                dynamic_sidebar('footer-newsletter');
            endif;
            ?>
        </div>
        <div id="navigation" class="grid_24">
            <div id="sitemap" class="grid_12 alpha">
                <h5>Site map</h5>
                <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
            </div>
            <div id="contact" class="grid_12 omega">
                <h5>Contact</h5>
                <div id="info">
                <?php
                if (is_active_sidebar('footer-contact')) :
                    dynamic_sidebar('footer-contact');
                endif;
                ?>
                </div>
            </div>
        </div>
        <div id="copyright" class="grid_24">
            <a href="privacy-policy">Privacy Policy</a>
            <span>© 2013 Ifeanyi Ubah - All Rights Reserved</span>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
