	<?php if ( is_active_sidebar( 'sidebar-posts' ) ) : ?>
		<div id="secondary" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar-posts' ); ?>
		</div><!-- #secondary -->
	<?php endif; ?>