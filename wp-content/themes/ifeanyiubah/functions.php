<?php

function ifeanyiubah_setup() {
	register_nav_menu( 'primary', __( 'Navigation Menu', 'ifeanyiubah' ) );

	add_theme_support( 'post-thumbnails' );

	set_post_thumbnail_size( 238, 109, true );

	add_image_size( 'header-image', 960, 280, true );
}
add_action( 'after_setup_theme', 'ifeanyiubah_setup' );


/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function ifeanyiubah_widgets_init() {

/************* HEADER *********************/

    register_sidebar( array(
      'id'            => 'header-social-media',
      'name'          => __( 'Header - Social Media', 'ifeanyiubah' ),
      'before_widget'  => '',                  
    ) );

    register_sidebar( array(
      'id'            => 'header-newsletter',
      'name'          => __( 'Header - Newsletter', 'ifeanyiubah' ),
      'before_widget'  => '',                  
    ) );

    register_sidebar( array(
      'id'            => 'header-main-text',
      'name'          => __( 'Header - Main Text', 'ifeanyiubah' ),
      'before_widget'  => '',                  
    ) );    

    register_sidebar( array(
      'id'            => 'homepage-video',
      'name'          => __( 'Homepage - Video', 'ifeanyiubah' ),
      'before_widget'  => '',                  
    ) );   

    register_sidebar( array(
      'id'            => 'footer-newsletter',
      'name'          => __( 'Footer - Newsletter', 'ifeanyiubah' ),  
      'before_widget'  => '',                  
    ) );    

    register_sidebar( array(
      'id'            => 'footer-contact',
      'name'          => __( 'Footer - Contact', 'ifeanyiubah' ),  
      'before_widget'  => '',                  
    ) );  

    register_sidebar( array(
      'id'            => 'sidebar-posts',
      'name'          => __( 'Sidebar - Posts', 'ifeanyiubah' ),  
      'before_widget'  => '',                  
    ) );           
}
add_action( 'widgets_init', 'ifeanyiubah_widgets_init' );

/************* SOCIAL LINKS *****************/

class SocialLinkWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Social Link','description=Social Link');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'class' => '', 'link' => '' ) );
			    $class = $instance['class'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('class'); ?>">CSS Class: </label><input class="widefat" id="<?php echo $this->get_field_id('class'); ?>" name="<?php echo $this->get_field_name('class'); ?>" type="text" value="<?php echo attribute_escape($class); ?>"/></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['class'] = $new_instance['class'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $class = empty($instance['class']) ? '' : apply_filters('widget_title', $instance['class']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

		    if (!empty($link))
		      echo "<li class='".$class."'><a href='".$link."' target='_blank'></a></li>";

		    echo $after_widget;
        }

}
register_widget( 'SocialLinkWidget' );

/************* HEADER MAIN TEXT *****************/

class MainTextWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Header Main Text','description=Header Main Text');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'subtitle' => '', 'text' => '', 'link' => '' ) );
			    $title = $instance['title'];
			    $subtitle = $instance['subtitle'];
			    $text = $instance['text'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>

			  <p><label for="<?php echo $this->get_field_id('subtitle'); ?>">Subtitle: <input class="widefat" id="<?php echo $this->get_field_id('subtitle'); ?>" name="<?php echo $this->get_field_name('subtitle'); ?>" type="text" value="<?php echo attribute_escape($subtitle); ?>" /></label></p>

			  <p><label for="<?php echo $this->get_field_id('text'); ?>">Text: </label><textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" /><?php echo attribute_escape($text); ?></textarea></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Read More Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['title'] = $new_instance['title'];
		    $instance['subtitle'] = $new_instance['subtitle'];
		    $instance['text'] = $new_instance['text'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
		    $subtitle = empty($instance['subtitle']) ? '' : apply_filters('widget_title', $instance['subtitle']);
		    $text = empty($instance['text']) ? '' : apply_filters('widget_title', $instance['text']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

		    if (!empty($title))
		      echo "<h2>".$title."</h2>";

		    if (!empty($subtitle))
		      echo "<span>".$subtitle."</span>";

		    if (!empty($text))
		      echo "<p>".$text."</p>";

		    if (!empty($link))
		      echo "<a href='".$link."'>Read More</a>";

		    echo $after_widget;
        }

}
register_widget( 'MainTextWidget' );

/************* CONTACT IN FOOTER *****************/

class FooterContactWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Footer Contact Info','description=Footer Contact Info');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'phone' => '', 'email' => '' ) );
			    $phone = $instance['phone'];
			    $email = $instance['email'];
			?>
			  <p><label for="<?php echo $this->get_field_id('phone'); ?>">Phone: </label><input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo attribute_escape($phone); ?>"/></p>

			  <p><label for="<?php echo $this->get_field_id('email'); ?>">Email: </label><input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo attribute_escape($email); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['phone'] = $new_instance['phone'];
		    $instance['email'] = $new_instance['email'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $phone = empty($instance['phone']) ? '' : apply_filters('widget_title', $instance['phone']);
		    $email = empty($instance['email']) ? '' : apply_filters('widget_title', $instance['email']);

		    if (!empty($phone))
		      echo "<p>Tel: ".$phone."</p>";

		    if (!empty($email))
		      echo "<a target='_blank' href='mailto:".$email."?Subject=Ifeanyi%20Ubah%20Foundation%20Contact'>".$email."</a>";

		    echo $after_widget;
        }

}
register_widget( 'FooterContactWidget' );

// Recent Post modification includes news custom post type

class WP_Widget_Recent_Posts2 extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_entries', 'description' => __( "The most recent posts and news on your site") );
		parent::__construct('recent-posts2', __('Recent Posts 2'), $widget_ops);
		$this->alt_option_name = 'widget_recent_entries';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = wp_cache_get('widget_recent_posts2', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
		if ( ! $number )
 			$number = 10;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		$r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true, 'post_type' => array( 'post', 'news' ) ) ) );
		if ($r->have_posts()) :
?>
		<?php echo $before_widget; ?>
		<?php if ( $title ) echo $before_title . $title . $after_title; ?>
		<ul>
		<?php while ( $r->have_posts() ) : $r->the_post(); ?>
			<li>
				<a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() ? get_the_title() : get_the_ID() ); ?>"><?php if ( get_the_title() ) the_title(); else the_ID(); ?></a>
			<?php if ( $show_date ) : ?>
				<span class="post-date"><?php echo get_the_date(); ?></span>
			<?php endif; ?>
			</li>
		<?php endwhile; ?>
		</ul>
		<?php echo $after_widget; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('widget_recent_posts2', $cache, 'widget');
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('widget_recent_posts2', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?' ); ?></label></p>
<?php
	}
}

register_widget('WP_Widget_Recent_Posts2');

function comment_content_filter($content){
	return '<div class="comment-text">“'.$content.'”</div>';
}

// COMMENTS REFORMATED FUNCTION
function ifeanyi_comment($comment, $args, $depth){
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
?>
		<<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
		<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
		<?php endif; ?>
		<div class="comment-author vcard">
		<?php echo get_avatar( $comment, 96 ); ?>
		</div>
<?php if ($comment->comment_approved == '0') : ?>
		<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
		<br />
<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
			?>
		</div>

		<div class="content">
			<?php add_filter('get_comment_text','comment_content_filter'); ?>
			<?php echo get_comment_text(); ?>

			<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
		</div>

		<div class="reply">
		<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>
		<?php if ( 'div' != $args['style'] ) : ?>
		</div>
		<?php endif; ?>
<?php	
}
